const path = require('path');
const gulp = require('gulp');
const babel = require('gulp-babel');
const cleanCss = require('gulp-clean-css');
const less = require('gulp-less');
const rename = require('gulp-rename');

const dist = path.join(__dirname, '../es');
const src = path.join(__dirname, '../src');
const extTypes = ['acss', 'axml', 'js', 'json'];

gulp.task('acss', () => {
  gulp
    .src(`${src}/**/*.less`)
    .pipe(less())
    .pipe(cleanCss())
    .pipe(
      rename({
        extname: '.acss',
      }),
    )
    .pipe(gulp.dest(dist));
});

gulp.task('axml', () => {
  gulp.src(`${src}/**/*.axml`).pipe(gulp.dest(dist));
});

gulp.task('js', () => {
  gulp
    .src(`${src}/**/*.js`)
    .pipe(babel())
    .on('error', (err) => {
      console.log(err);
    })
    .pipe(gulp.dest(dist));
});

gulp.task('json', () => {
  gulp.src(`${src}/**/*.json`).pipe(gulp.dest(dist));
});

gulp.task('build', extTypes);
gulp.start('build');
