export default {
  advertiseUrl: 'https://qn1.topchitu.com/advertise/miniAppMobile3.html',
  indexPage: '/pages/index/index',
  defaultHtmlAdvertiseId: 'web-view-html-advertise',
  defaultHtmlAdvertiseCode: 'adMobileHtmlPaymentAD',
  defaultHtmlAdvertiseEmptyContent: '<div style="display: flex;height: 100%;justify-content:  center;align-items: center;"><div style="width: 90%;align-items: center;justify-content:  center;display: flex;"><img style="width:100%;flex: 1;" src="https://img.alicdn.com/imgextra/i3/373575809/TB2wkdkllfH8KJjy1XbXXbLdXXa-373575809.png" /></div></div>',
  APP_SHOW_LOADED_ADVERTISE_EVENT: 'APP_SHOW_LOADED_ADVERTISE',
  timeOutPattern: /mini-app-mobile-timeout="(\d+)"/,
  heightPattern: /mini-app-height="(.*?)"/,
  /**
   * 分配给广告组件使用的localStorage key
   */
  preventedLocalStoreKeys: {
    htmlAdvertiseLocalStorage: 'htmlAdvertiseLocalStorage',
    advertise: 'advertise',
  },
  onHtmlAdvertiseUnLoad: (entry = false) => {
    console.log(`onHtmlAdvertiseUnLoad entry=${entry}`);
  },
  /**
   * 获取广告位内容的接口
   */
  getAds: async (advertiseCode) => {
    console.log('advertiseCode :>> ', advertiseCode);
    return advertiseCode;
  },
  /**
   * 广告操作对应点额处理函数
   */
  htmlAdvertiseEventHandlerConfig: {
    chatWithChitu(chatMessage) {
      console.log('chatMessage :>> ', chatMessage);
    },
    chatToWw(nick, chatMessage) {
      console.log(`chatToWW ${nick} with ${chatMessage}`);
    },
    /**
     * @param {*} data 当前页面的data数据
     */
    closeHtmlAd(data = {}) {
      console.log('close html advertise', data);
    },
    jumpToWeb(url) {
      console.log(`will jump to ${url}`);
    },
  },
  swiperAdvertise: {
    defaultCode: 'adMobileHtmlNewGuide',
    errorLog: (p1, p2, p3) => console.log(p1, p2, p3),
    jumpTo: (href) => console.log(`jump to ${href}`),
    chat: (name, content) => console.log(`chat ${name} with ${content}`),
  },
};
