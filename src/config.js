/**
 * 所有定制化操作从这里配置, 具体配置见 advertiseConfiguration.js
 */
import configuration from './advertiseConfiguration';
import _ from 'lodash-es';

function config(path, val) {
  if (_.isUndefined(path) || _.isUndefined(val)) {
    return;
  }
  _.set(configuration, path, val);
  return { config };
}

export default {
  config,
};
